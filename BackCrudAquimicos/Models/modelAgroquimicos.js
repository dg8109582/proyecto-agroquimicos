const mongoose = require('mongoose');
let Schema = mongoose.Schema;

//Modelo a utilizar en la base de datos
let agroquimicoNuevo = new Schema({
    nombre: { type: String },
    tipo: { type: String },
    tamano: { type: String },
    precio: { type: Number },
    descripcion: { type: String },
    img: { type: String },
   
});


module.exports = mongoose.model('agroquimicos', agroquimicoNuevo);
