const express = require('express')
const app = express();

app.get('/prueba', (req, resp) => {
    return resp.json({
        ok: true,
        message: 'Todo Corriendo',
    });
});

module.exports = app;