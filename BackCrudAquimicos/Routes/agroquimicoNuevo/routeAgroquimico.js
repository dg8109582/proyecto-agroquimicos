const { Router } = require('express');
const express = require('express');
const { db } = require('../../Models/modelAgroquimicos');
const modelAgroquimicos = require('../../Models/modelAgroquimicos');

let app = express();
//inicio metodo post
app.post('/nuevo/agroquimico', (req, res) => {
    let body = req.body;
    console.log(body);
    let newSchemaAgroquimico = new modelAgroquimicos({
        nombre: body.nombre,
        tipo: body.tipo,
        tamano: body.tamano,
        precio: body.precio,
        descripcion: body.descripcion,
        img: body.img,
        
    });
    newSchemaAgroquimico
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos Guardados con éxito',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Oh no.. ha ocurrido un error',
                        err
                    });
            }
        );

});
//Fin metodo post
//inicio metodo get
app.get('/todos/agroquimicos', async(req, res)=>{
    await modelAgroquimicos
        .find()
        .then(
            (data)=>{
                console.log(data);
                return res.status(200)
                .json({
                    ok: true,
                    message: 'Datos encontrados con exito',
                    data
                });
            }
        )
        .catch(
            (err)=>{
                return res.status(500)
                .json({
                    ok: false,
                    message: 'oh no... ha ocurrido un error',
                    err
                });
            }
        );
})
//Fin metodo get
//inicio metodo put
app.put('/cambiar/agroquimico/:id', async(req, res) =>{
    const{
        nombre,
        tipo,
        tamano,
        precio,
        descripcion,
        img}=req.body;

    let agroquimico= await modelAgroquimicos
        .findById(req.params.id)

        agroquimico.nombre= nombre;
        agroquimico.tipo= tipo;
        agroquimico.tamano= tamano;
        agroquimico.precio=precio;
        agroquimico.descripcion=descripcion;
        agroquimico.img=img;
        
        

        agroquimico=await modelAgroquimicos.findOneAndUpdate({_id: req.params.id},agroquimico,{new:true})

        .then(
            (data) => {
                console.log(data);
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Se ha actualizado correctamente',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'error... verifica tu error',
                        err
                    });
            }
        );
})

//fin metodo put
//inicio metodo delete

app.delete('/borrar/agroquimico/:id', async(req, res) =>{
    let agroquimico = await modelAgroquimicos
        .findById(req.params.id)

        await modelAgroquimicos.findOneAndRemove({_id: req.params.id})
        .then(
            (data) => {
                console.log(data);
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Se ha eliminado el registro correctamente',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Error, verifica tu error',
                        err
                    });
            }
        );
})

//fin metodo delete




module.exports = app;