const mongoose = require('mongoose');

const app = require('../Server/index');
const port = 3900;

// Generar promesa global para Mongoose
mongoose.Promise = global.Promise;

//Conexion a MongoDB
mongoose.connect('mongodb://localhost:27017/agroquimicos', { useNewUrlParser: true })
    .then(() => {
        console.log('Base corriendo en Local')

        //Escuchar al servidor
        app.listen(port, () => {
            console.log(`Server corriendo en el puerto: ${port}`);
        })
    });